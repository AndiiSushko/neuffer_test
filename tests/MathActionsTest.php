<?php


use NeufferTest\Classes\MathActions;
use PHPUnit\Framework\TestCase;

class MathActionsTest extends TestCase
{
    public function testPlusMethod()
    {
        $this->assertEquals(10, MathActions::plus(5,5));
    }

    public function testMinusMethod()
    {
        $this->assertEquals(10, MathActions::minus(15,5));
    }

    public function testMultiplyMethod()
    {
        $this->assertEquals(25, MathActions::multiply(5,5));
    }

    public function testDivisionMethod()
    {
        //Positive flow
        $this->assertEquals(5, MathActions::division(25,5));

        //Division by zero exception
        $this->expectException(Exception::class);
        $this->assertEquals(5, MathActions::division(25,0));
    }
}
