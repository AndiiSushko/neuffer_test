<?php


use NeufferTest\Classes\CsvProcessor;
use NeufferTest\Classes\Exceptions\WrongActionException;
use NeufferTest\Classes\Exceptions\WrongInputFileException;
use NeufferTest\Classes\Files\LogFile;
use NeufferTest\Classes\Files\OutputFile;
use PHPUnit\Framework\TestCase;

include_once __DIR__ .'/../Classes/CsvProcessor.php';

class csvProcessorTest extends TestCase
{
    private static function runPrivateMethod($object, $methodName, $args = []) {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }

    private static function removeOutputFiles()
    {
        if (file_exists(OutputFile::OUTPUT_FILE_NAME)){
            unlink(OutputFile::OUTPUT_FILE_NAME);
        }

        if (file_exists(LogFile::LOG_FILE_NAME)){
            unlink(LogFile::LOG_FILE_NAME);
        }
    }

    public function testCsvProcessorConstructor()
    {
        // positive flow
        // I only test here one right action because other actions will be tested in testSelectAction
        $csvProcessor = new CsvProcessor('plus', 'test.csv');
        $this->assertInstanceOf(CsvProcessor::class, $csvProcessor);

        //with wrong action
        $this->expectException(WrongActionException::class);
        $csvProcessor = new CsvProcessor('wrongAction', 'test.csv');
        $this->assertInstanceOf(CsvProcessor::class, $csvProcessor);

        //with wrong file name
        $this->expectException(WrongInputFileException::class);
        $csvProcessor = new CsvProcessor('plus', 'wrongtest.csv');
        $this->assertInstanceOf(CsvProcessor::class, $csvProcessor);
    }

    public function testSelectAction()
    {
        // This test can be done by creating new instances and passing action to constructor
        // but I want to check selectAction separate to stay close to Unit tests.

        // good methods
        $csvProcessor = new CsvProcessor('plus', 'test.csv');
        foreach(['plus', 'minus', 'multiply', 'division'] as $method){
            $selected = self::runPrivateMethod($csvProcessor, 'selectAction', [$method]);
            $this->assertEquals($method, $selected);
        }

        //bad method
        $this->expectException(WrongActionException::class);
        self::runPrivateMethod($csvProcessor, 'selectAction', ['wrongMethodName']);
    }

    public function testProcess()
    {
        // remove output files before process
        self::removeOutputFiles();

        foreach(['plus', 'minus', 'multiply', 'division'] as $method) {
            $csvProcessor = new CsvProcessor($method, 'test.csv');
            // checking, if run is done without exceptions and return correct line count
            $this->assertEquals(2000, $csvProcessor->process());
        }

        // check if output and log files are created
        $this->assertTrue(file_exists(OutputFile::OUTPUT_FILE_NAME));
        $this->assertTrue(file_exists(LogFile::LOG_FILE_NAME));
    }
}
