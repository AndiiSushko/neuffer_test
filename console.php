<?php

// Hide warnings in console output. Using exception messages instead.
use NeufferTest\Classes\CsvProcessor;

error_reporting(E_ALL);

include_once 'Classes/CsvProcessor.php';

$longopts  = array(
    "action:",
    "file:",
);

$options = getopt('', $longopts);

if (!isset($options['action']) || !isset($options['file'])){
    die('Please provide both options, --action and --file' . PHP_EOL);
}

try{
    $linesProcessed = (new CsvProcessor($options['action'], $options['file']))->process();
    die("$linesProcessed lines was processed" . PHP_EOL);
}catch (\Exception $exception){
    die($exception->getMessage() . PHP_EOL);
}