<?php


namespace NeufferTest\Classes\Files;

use NeufferTest\Classes\Exceptions\WrongInputFileException;

include_once 'Classes/Files/File.php';
include_once  'Classes/Exceptions/WrongInputFileException.php';

class SourceFile extends File
{
    public function __construct($fileName){
        if(!$this->filePointer = fopen($fileName, 'r')){
            throw new WrongInputFileException($fileName);
        };
    }

    public function getNextLine() : string
    {
        return fgets($this->filePointer, 1000);
    }
}