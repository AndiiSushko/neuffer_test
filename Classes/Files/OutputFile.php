<?php


namespace NeufferTest\Classes\Files;

include_once 'Classes/Files/File.php';

class OutputFile extends File
{
    public const OUTPUT_FILE_NAME = 'output.csv';

    public function __construct()
    {
        $this->filePointer = fopen(self::OUTPUT_FILE_NAME, 'w');
    }

    public function writeOut(string $line) : void
    {
        fwrite($this->filePointer, $line . PHP_EOL);
    }
}