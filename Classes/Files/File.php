<?php


namespace NeufferTest\Classes\Files;


abstract class File
{
    protected $filePointer;

    public function __destruct()
    {
        fclose($this->filePointer);
    }
}