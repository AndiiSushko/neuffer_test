<?php

namespace NeufferTest\Classes\Files;

include_once 'Classes/Files/File.php';

class LogFile extends File
{
    public const LOG_FILE_NAME = 'process.log';

    public function __construct()
    {
        $this->filePointer = fopen(self::LOG_FILE_NAME, 'w');
    }

    public function log(string $message) : void
    {
        fwrite($this->filePointer, $message . PHP_EOL);
    }
}