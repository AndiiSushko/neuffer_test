<?php

namespace NeufferTest\Classes;

use NeufferTest\Classes\Exceptions\WrongActionException;
use NeufferTest\Classes\Files\LogFile;
use NeufferTest\Classes\Files\OutputFile;
use NeufferTest\Classes\Files\SourceFile;

include_once 'Files/LogFile.php';
include_once 'Files/OutputFile.php';
include_once 'Files/SourceFile.php';
include_once 'Exceptions/WrongActionException.php';
include_once 'MathActions.php';

class CsvProcessor
{
    private $action;

    private $logger;
    private $source;
    private $output;

    public function __construct(string $action, string $fileName)
    {
        $this->action = $this->selectAction($action);

        $this->logger = new LogFile();
        $this->source = new SourceFile($fileName);
        $this->output = new OutputFile();
    }

    public function process() : int
    {
        $lineCounter = 0;
        while ($line = $this->source->getNextLine()) {

            $lineCounter ++;
            $lineArr = explode(';', $line);
            if (count($lineArr) != 2){
                throw new \Exception('Wrong csv format on line ' . $lineCounter);
            }

            $x = intval($lineArr[0]);
            $y = intval($lineArr[1]);

            try{
                $answer = MathActions::{$this->action}($x, $y);
            }catch (\Exception $exception){
                $this->logger->log($exception->getMessage());
                // use this to add current numbers to log
                $answer = -1;
            }

            if ($answer > 0){
                $this->output->writeOut(implode(' ', [$x, $y, $answer]));
            }else {
                $this->logger->log("Numbers $x and $y are wrong");
            }
        }

        return $lineCounter;
    }

    // This method does nothing now, but can be used in future to implement more complex logic
    // and also in this way method can be tested.
    private function selectAction($action) : string
    {
        // Right now it return the same value, but can be used if we need a difference between
        // action names and method names. break; also used as part of "good practices".
        switch ($action){
            case 'plus':
                return 'plus';
                break;
            case 'minus':
                return 'minus';
                break;
            case 'multiply':
                return 'multiply';
                break;
            case 'division':
                return 'division';
                break;
        }

        throw new WrongActionException($action);
    }
}