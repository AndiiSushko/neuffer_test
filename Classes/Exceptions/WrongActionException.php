<?php

namespace NeufferTest\Classes\Exceptions;

class WrongActionException extends \Exception
{
    public function errorMessage($wrongActionName) : string
    {
        return "Action $wrongActionName doesn't exist.";
    }
}