<?php

namespace NeufferTest\Classes\Exceptions;

class WrongInputFileException extends \Exception
{
    public function errorMessage($wrongFileName) : string
    {
        return "File $wrongFileName doesn't exist.";
    }
}