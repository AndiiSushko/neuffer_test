<?php

namespace NeufferTest\Classes;

// This class don't have any state, we don't need instances, so all methods are set as static.
class MathActions
{
    public static function plus(int $x, int $y) : int
    {
        return $x + $y;
    }

    public static function minus(int $x, int $y) : int
    {
        return $x - $y;
    }

    public static function multiply(int $x, int $y) :int
    {
        return $x * $y;
    }

    public static function division(int $x, int $y) : float
    {
        if ($y === 0){
            throw new \Exception('Division by zero is prohibited. Will be enabled in the next release.');
        }

        return $x/$y;
    }
}